<?php

return [

    'title' => 'Preçário',
    'yearly' => 'anual',
    'monthly' => 'mensal',
    'vip' => [
        'title' => "VIP",
        'features' => [
            "Tag [VIP] no Chat e Scoreboard",
            "Menu de Sons ➜ !sons",
            "Menu de Models/Luvas ➜ !models",
            "Menu de Danças do Fortnite ➜ !dancas",
            "Menu de Armas Customizadas ➜ !armas",
            "Alterar o fundo do mapa ➜ !skybox",
            "Recebe o sêxtuplo (6x) créditos para gastar na loja!"
        ]
    ],
    'sponsor' => [
        'title' => "SPONSOR",
        'features' => [
            "Tag [Sponsor] no Chat e Scoreboard",
            "Vantagens de VIP",
            "Slay/Kick/Ban",
            "Mute/Gag",
        ]
    ],
    'buy_now' => 'Comprar agora'

];