<?php

return [

    'title' => 'A nossa Equipa',
    'roles' => [
        'founder' => 'Fundador',
        'admin' => 'Admin',
        'mod' => 'Moderador',
        'helper' => 'Helper',
        'sponsor' => 'Sponsor'
    ],
];