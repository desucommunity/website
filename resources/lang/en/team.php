<?php

return [

    'title' => 'Our Team',
    'roles' => [
        'founder' => 'Founder',
        'admin' => 'Admin',
        'mod' => 'Moderator',
        'helper' => 'Helper',
        'sponsor' => 'Sponsor'
    ],
];