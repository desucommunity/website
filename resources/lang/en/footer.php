<?php

return [

    'copyright' => 'Copyright © :year. ALL RIGHTS RESERVED.',
    'design' => 'DESIGNED BY',
    'develop' => 'DEVELOPED BY'
];