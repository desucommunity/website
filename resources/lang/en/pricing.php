<?php

return [

    'title' => 'Pricing',
    'yearly' => 'yearly',
    'monthly' => 'monthly',
    'vip' => [
        'title' => "VIP",
        'features' => [
            "[VIP] Tag on Chat and Scoreboard",
            "Sounds Menu ➜ !sons",
            "Models/Gloves Menu ➜ !models",
            "Fortnite Dances Menu ➜ !dances",
            "Custom Weapons Menu ➜ !cw",
            "Skybox Menu ➜ !skybox",
            "Earn 6x credits per minute to spend on shop!"
        ]
    ],
    'sponsor' => [
        'title' => "SPONSOR",
        'features' => [
            "[Sponsor] Tag on Chat and Scoreboard",
            "VIP Features",
            "Slay/Kick/Ban",
            "Mute/Gag",
        ]
    ],
    'buy_now' => 'Purchase now'

];