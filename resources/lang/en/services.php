<?php

return [

    'title' => 'What We Offer',
    'features' => [
        ["icon" => "fa-diamond", "title" => "App Development", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
        ["icon" => "fa-rocket", "title" => "Graphic Design", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
        ["icon" => "fa-cogs", "title" => "Creative Idea", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
        ["icon" => "fa-diamond", "title" => "Marketing", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
        ["icon" => "fa-pencil", "title" => "Awesome Support", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
        ["icon" => "fa-flask", "title" => "Brand Design", "description" => "Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero."],
    ]
];