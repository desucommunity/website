<?php

return [

    'title' => 'Oops! We can’t find the page you’re looking for.',
    'home' => 'Go Home'
];