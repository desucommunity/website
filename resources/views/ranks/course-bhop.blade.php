@extends('layouts.app')

@section('title', 'Ranks Courses/Bhop')

@section('content')
    @include('layouts.nav', ['title' => "Courses/Bhop", 'breadcrumb' => "Courses/Bhop", "after_breadcrumb" => "Ranks"])
    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <!-- Main -->
            <main id="main" class="col-md-12">
                <div class="section md-padding">
                    @auth("player")
                        @include("ranks.own-coursebhop", ['player' => $playerLogin, 'user' => auth()->guard('player')->user(), 'numRows' => $numRows])
                    @else
                        <p><strong>{{ __('ranks.guest') }} <a href="/login/steam">login</a>.</strong></p>
                    @endauth
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="text-lg-left text-center">{{ __('ranks.name') }}</th>
                                <th scope="col" class="text-lg-left text-center">{{ __('ranks.points') }}</th>
                                <th scope="col" class="hidden-xs text-center">Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($players as $key => $player)
                                <tr>
                                    <td>#{{ ($page*10)+$key+1 }}</td>
                                    <td class="text-lg-left text-center">{{ $player->user->name }}</td>
                                    <td class="text-center">{{ $player->cachedpoints }}</td>
                                    <td class="text-center">{{ $player->rank }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $players->links() }}
                </div>
            </main>
            <!-- /Main -->

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->
@endsection