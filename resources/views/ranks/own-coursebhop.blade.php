<div class="row bg-dark my-rank-stats">
    @if($player != null)
    <div class="col-md-3">
        <img class="img-circle img-responsive" src="{{ $user->avatar }}" alt="Steam Image">
    </div>
    <div class="col-md-8">
        <h3 class="text-white d-inline-block">{{ $player->user->name }}</h3>
    </div>
        
    <div class="col-md-3">
        <label>{{ __('ranks.points') }}:</label>
        {{ $player->cachedpoints }}
    </div>
    <div class="col-md-3">
        <label>Rank:</label>
        {{ $player->rank }}
    </div>
    @else
        <p class="text-white"><strong>Precisas de jogar no servidor primeiro, para veres as tuas estatísticas.</strong></p>
    @endif
</div>