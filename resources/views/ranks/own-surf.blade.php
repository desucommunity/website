<div class="row bg-dark my-rank-stats">
    
    <div class="col-md-3">
        <img class="img-circle img-responsive" src="{{ $user->avatar }}" alt="Steam Image">
    </div>
    <div class="col-md-8">
        <h3 class="text-white d-inline-block">{{ $player->name }}</h3>
        <!--<h2 class="text-white d-inline-block" style="padding-left: 20px;">2/{{ $numRows }}</h2>-->
    </div>
        
    <div class="col-md-3">
        <img src="{{ asset('img/ranks/'.$player->RankImage) }}" alt="Rank"/>
        <label>{{ __('ranks.points') }}:</label>
        {{ $player->score }}
    </div>
    <div class="col-md-3">
        <label>KDR:</label>
        {{ $player->KDR }}
    </div>
    <div class="col-md-3">
        <label>HeadShots:</label>
        {{ $player->headshots }}
    </div>
    <div class="col-md-3">
        <label>NoScopes:</label>
        {{ $player->no_scope }}
    </div>
    <div class="col-md-3">
        <label>KDA:</label>
        {{ $player->kills }}/{{ $player->deaths }}/{{ $player->assists }}
    </div>
</div>