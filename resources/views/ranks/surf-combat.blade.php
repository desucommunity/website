@extends('layouts.app')

@section('title', 'Ranks Surf Combat')

@section('content')
    @include('layouts.nav', ['title' => "Surf Combat", 'breadcrumb' => "Surf Combat", "after_breadcrumb" => "Ranks"])
    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <!-- Main -->
            <main id="main" class="col-md-12">
                <div class="section md-padding">
                    @auth("player")
                        @include("ranks.own-surf", ['player' => $playerLogin, 'user' => auth()->guard('player')->user(), 'numRows' => $numRows])
                    @else
                        <p><strong>{{ __('ranks.guest') }} <a href="/login/steam">login</a>.</strong></p>
                    @endauth
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="text-lg-left text-center">{{ __('ranks.name') }}</th>
                                <th scope="col" class="text-lg-left text-center">{{ __('ranks.points') }}</th>
                                <th scope="col" class="hidden-xs text-center">KDR</th>
                                <th scope="col" class="hidden-xs text-center">Headshots</th>
                                <th scope="col" class="hidden-xs text-center">NoScopes</th>
                                <th scope="col" class="hidden-xs text-center">Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($players as $key => $player)
                                <tr>
                                    <td>#{{ ($page*10)+$key+1 }}</td>
                                    <td class="text-lg-left text-center">{{ $player->name }}</td>
                                    <td class="text-center">{{ $player->score }}</td>
                                    <td class="hidden-xs text-center">{{ $player->KDR }}</td>
                                    <td class="hidden-xs text-center">{{ $player->headshots }}</td>
                                    <td class="hidden-xs text-center">{{ $player->no_scope }}</td>
                                    <td class="hidden-xs text-center"><img src="{{ asset('img/ranks/'.$player->RankImage) }}" alt="Rank"/></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $players->links() }}
                </div>
            </main>
            <!-- /Main -->

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->
@endsection