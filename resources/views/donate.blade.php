@extends('layouts.app')

@section('title', __('welcome.donate'))

@section('content')
    @include('layouts.nav', ['title' => __('welcome.donate'), 'breadcrumb' => __('welcome.donate')])
    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <!-- Main -->
            <main id="main" class="col-md-12">
                <!-- Pricing -->
                <div id="pricing" class="section">

                    <!-- Container -->
                    <div class="container">

                        <!-- Row -->
                        <div class="row">

                            <!-- pricing -->
                            <div class="col-sm-6">
                                <div class="pricing">
                                    <div class="price-head">
                                        <span class="price-title">{{ __('pricing.vip.title') }}</span>
                                        <div class="price">
                                            <h3>{{ env('VIP_PRICE') }}€<span class="duration">/ {{ __('pricing.yearly') }}</span></h3>
                                        </div>
                                    </div>
                                    <ul class="price-content">
                                        @foreach(__('pricing.vip.features') as $feature)
                                            <li>
                                                <p>{{ $feature }}</p>
                                            </li>
                                        @endforeach
                                        <li>
                                            <p>...</p>
                                        </li>
                                    </ul>
                                    <div class="price-btn">
                                        <a target="_blank" href="https://steamcommunity.com/groups/DesuCommunity/discussions/0/4379130600468097088/">
                                            <button class="outline-btn">{{ __('pricing.buy_now') }}</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- /pricing -->

                            <!-- pricing -->
                            <div class="col-sm-6">
                                <div class="pricing sponsor">
                                    <div class="price-head">
                                        <span class="price-title">{{ __('pricing.sponsor.title') }}</span>
                                        <div class="price">
                                            <h3>{{ env('SPONSOR_PRICE') }}€<span class="duration">/ {{ __('pricing.monthly') }}</span></h3>
                                        </div>
                                    </div>
                                    <ul class="price-content">
                                        @foreach(__('pricing.sponsor.features') as $feature)
                                            <li>
                                                <p>{{ $feature }}</p>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="price-btn">
                                        <a target="_blank" href="https://steamcommunity.com/groups/DesuCommunity/discussions/0/2796124117961472513/">
                                            <button class="outline-btn">{{ __('pricing.buy_now') }}</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- /pricing -->

                        </div>
                        <!-- Row -->

                    </div>
                    <!-- /Container -->

                </div>
                <!-- /Pricing -->
            </main>
            <!-- /Main -->

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->
@endsection