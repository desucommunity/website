@extends('layouts.app')

@section('title', 'Donate')

@section('content')
    @include('layouts.nav', ['title' => "404", 'breadcrumb' => "404"])
    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <!-- Main -->
            <main id="main" class="col-md-12">
                
                <div class="section md-padding">
                    <h2>{{ __('404.title') }}</h2>
                    <h3><a href="/">{{ __('404.home') }}</a></h3>
                </div>
                
            </main>
            <!-- /Main -->

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->
@endsection