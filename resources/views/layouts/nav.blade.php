<!-- Header -->
<header>

<!-- Nav -->
<nav id="nav" class="navbar">
    <div class="container">

        <div class="navbar-header">
            <!-- Logo -->
            <div class="navbar-brand">
                <a href="/">
                    <img class="logo" src="{{ asset('img/logo.png') }}" alt="logo">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Collapse nav button -->
            <div class="nav-collapse">
                <span></span>
            </div>
            <!-- /Collapse nav button -->
        </div>

        <!--  Main navigation  -->
        <ul class="main-nav nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li class="{{ Request::is('donate') ? 'active' : '' }}"><a href="{{ route('donate') }}">{{__('welcome.donate')}}</a></li>
            <li class="{{ Request::is('bans') ? 'active' : '' }}"><a href="https://desucm.tk/bans/" target="_blank">Bans</a></li>
            <li class="has-dropdown {{ Request::is('ranks*') ? 'active' : '' }}"><a>Ranks</a>
                <ul class="dropdown">
                    <li><a href="{{ route('ranks.courses-bhop') }}">Courses/Bhop</a></li>
                    <li><a href="{{ route('ranks.surf-combat') }}">Surf Combat</a></li>
                </ul>
            </li>
            <li class="has-dropdown"><a>{{ strtoupper(app()->getLocale()) }}</a>
                <ul class="dropdown dropdown-lang">
                    @if(app()->getLocale() == "pt")
                        <li><a href="/lang/en">EN</a></li>
                    @else
                        <li><a href="/lang/pt">PT</a></li>
                    @endif
                </ul>
            </li>
            @auth("player")
                <?php $player = auth()->guard('player')->user(); ?>
                <li class="has-dropdown"><a><strong>{{ __('nav.player') }}</strong> {{ $player->name }}</a>
                    <ul class="dropdown">
                        <li><a href="/logout">{{ __('nav.logout') }}</a></li>
                    </ul>
                </li>
            @else
                <li class="login">
                    <a href="/login/steam">
                        <img class="steam-login" width="160" height="25" src="/img/steam-login.png" alt="Steam Login">
                    </a>
                </li>
            @endauth
        </ul>
        <!-- /Main navigation -->

    </div>
</nav>
<!-- /Nav -->

<!-- header wrapper -->
<div class="header-wrapper sm-padding bg-grey">
    <div class="container">
        <h2>{{$title}}</h2>
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            @if(isset($after_breadcrumb))
            <li class="breadcrumb-item"><a href="#">{{ $after_breadcrumb }}</a></li>
            @endif
            <li class="breadcrumb-item active">{{ $breadcrumb }}</li>
        </ul>
    </div>
</div>
<!-- /header wrapper -->

</header>
<!-- /Header -->
