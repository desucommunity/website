<!-- Header -->
<header id="home">
        
		<!-- Background Image -->
		<div class="bg-img" style="background-image: url('/img/background1.jpg');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->

		<!-- Nav -->
		<nav id="nav" class="navbar nav-transparent">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="/">
							<img class="logo" src="{{ asset('img/logo.png') }}" alt="logo">
							<img class="logo-alt" src="{{ asset('img/logo-alt.png') }}" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li class="active"><a href="/">Home</a></li>
					<li><a href="{{ route('donate') }}">{{__('welcome.donate')}}</a></li>
					<li><a href="https://desucm.tk/bans/" target="_blank">Bans</a></li>
					<li class="has-dropdown"><a>Ranks</a>
						<ul class="dropdown">
							<li><a href="{{ route('ranks.courses-bhop') }}">Courses/Bhop</a></li>
							<li><a href="{{ route('ranks.surf-combat') }}">Surf Combat</a></li>
						</ul>
					</li>
					<li class="has-dropdown"><a>{{ strtoupper(app()->getLocale()) }}</a>
						<ul class="dropdown dropdown-lang">
							@if(app()->getLocale() == "pt")
								<li><a href="/lang/en">EN</a></li>
							@else
								<li><a href="/lang/pt">PT</a></li>
							@endif
						</ul>
					</li>
					@auth("player")
						<?php $player = auth()->guard('player')->user(); ?>
						<li class="has-dropdown"><a><strong>{{ __('nav.player') }}</strong> {{ $player->name }}</a>
							<ul class="dropdown">
								<li><a href="/logout">{{ __('nav.logout') }}</a></li>
							</ul>
						</li>
					@else
						<li class="login">
							<a href="/login/steam">
								<img class="steam-login" width="160" height="25" src="/img/steam-login.png" alt="Steam Login">
							</a>
						</li>
					@endauth
					
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

		@yield('home-wrapper')

	</header>
	<!-- /Header -->
