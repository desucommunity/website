<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>DesuCommunity - @yield('title')</title>

    <link rel="icon" type="image/jpg" href="{{ asset('img/favicon.jpg') }}"/>
    
    <meta name="description" content="A DesuCommunity foi criada inicialmente para fornecer aos jogadores, servidores únicos , pois não encontramos nenhum servidor que satisfazia as nossas preferências.">

    <meta name="robots" content="index, follow">

    <meta name="keywords" content="DesuCommunity,desucommunity,desu,community,comunidade,csgo,portugal,tuga,courses,bhop,surf,combat,csgo,vip,sponsor,fun">

    <meta name="author" content="Bernardino Sousa">

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/fork-awesome.min.css') }}">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    @yield('content')

    <!-- Footer -->
    <footer id="footer" class="sm-padding bg-dark">

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <div class="col-md-12">

                <!-- footer logo -->
                <div class="footer-logo">
                    <a href="./"><img src="{{ asset('img/logo-alt.png') }}" alt="logo"></a>
                </div>
                <!-- /footer logo -->

                <!-- footer follow -->
                <ul class="footer-follow">
                    <li><a target="_blank" href="http://steam.desucm.tk"><i class="fa fa-steam"></i></a></li>
                    <li><a target="_blank" href="http://discord.desucm.tk"><i class="fa fa-discord" aria-hidden="true"></i></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/channel/UC-PmpYpj6_YT75lFV-imBqQ"><i class="fa fa-youtube"></i></a></li>
                </ul>
                <!-- /footer follow -->

                <!-- footer copyright -->
                <div class="footer-copyright">
                    <p>
                        {{ __('footer.copyright', ['year' => now()->year]) }}
                        {{ __('footer.design') }} <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    </p>
                    <p>{{ __('footer.develop') }} <a href="https://bernardinosousa.site/" target="_blank">Bernardino Sousa</a></p>
                </div>
                <!-- /footer copyright -->

            </div>

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    </footer>
    <!-- /Footer -->

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- jQuery Plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{env('GOOGLEANALYTICS_ID')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{env("GOOGLEANALYTICS_ID")}}');
    </script>

</body>

</html>