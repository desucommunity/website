@extends('layouts.app')

@section('title', 'Homepage')

@section('home-wrapper')

    <!-- home wrapper -->
    <div class="home-wrapper">
        <div class="container">
            <div class="row">

                <!-- home content -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="home-content">
                        <h1 class="white-text">{{__('welcome.title')}}</h1>
                        <p class="white-text">{{__('welcome.subtitle')}}</p>
                        <!--<a href="#service"><button class="white-btn">{{ __('services.title') }}</button></a>-->
                        <a href="#servers"><button class="red-btn">{{ __('servers.title') }}</button></a>
                        <a href="/donate"><button class="main-btn">{{__('welcome.donate')}}</button></a>
                    </div>
                </div>
                <!-- /home content -->

            </div>
        </div>
    </div>
    <!-- /home wrapper -->

@endsection

@section('content')
    @include('layouts.nav-homepage')
        <!-- Service 
        <div id="service" class="section md-padding">

            
            <div class="container">

                
                <div class="row">

                    
                    <div class="section-header text-center">
                        <h2 class="title">{{ __('services.title') }}</h2>
                    </div>
                    

                    @foreach(__('services.features') as $feature)
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="service">
                            <i class="fa {{ $feature['icon'] }}"></i>
                            <h3>{{ $feature['title'] }}</h3>
                            <p>{{ $feature['description'] }}</p>
                        </div>
                    </div>
                    
                    @endforeach

                </div>

            </div>
            

        </div>
        -->

        <!-- Servers -->
        <div id="servers" class="section md-padding bg-grey">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- why choose us content -->
                <div class="col-md-12">
                    <div class="section-header">
                        <h2 class="title">{{ __('servers.title') }}</h2>
                    </div>
                    
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="hidden-xs"></th>
                                <th scope="col" class="text-lg-left text-center">{{ __('servers.server') }}</th>
                                <th scope="col" class="text-center">IP</th>
                                <th scope="col" class="hidden-xs text-center">{{ __('servers.map') }}</th>
                                <th scope="col" class="hidden-xs text-center">{{ __('servers.players') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($servers as $server)
                                <tr>
                                    <td class="hidden-xs"><img src="/img/icons/csgo.png"/></td>
                                    <td class="text-lg-left text-center">{{ $server->server_name }}</td>
                                    <td class="text-center"><a href="steam://{{ $server->IPAddress }}:{{ $server->server_port }}">{{ $server->IPAddress }}:{{ $server->server_port }}</a></td>
                                    <td class="hidden-xs text-center">{{ $server->server_map }}</td>
                                    <td class="hidden-xs text-center">{{ $server->number_of_players }}/{{ $server->max_players }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /why choose us content -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

        </div>
        <!-- /Why Choose Us -->


        <!-- Numbers -->
        <div id="numbers" class="section sm-padding">

        <!-- Background Image -->
        <div class="bg-img" style="background-image: url('./img/background2.jpg');">
            <div class="overlay"></div>
        </div>
        <!-- /Background Image -->

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- number -->
                <div class="col-sm-3 col-xs-6">
                    <div class="number">
                        <i class="fa fa-users"></i>
                        <h3 class="white-text"><span class="counter">{{ $steamGroupMembers }}</span></h3>
                        <span class="white-text">{{ __('numbers.members') }}</span>
                    </div>
                </div>
                <!-- /number -->

                <!-- number -->
                <div class="col-sm-3 col-xs-6">
                    <div class="number">
                        <i class="fa fa-users"></i>
                        <h3 class="white-text"><span class="counter">{{ $playersOnline }}</span></h3>
                        <span class="white-text">{{ __('numbers.players_online') }}</span>
                    </div>
                </div>
                <!-- /number -->

                <!-- number -->
                <div class="col-sm-3 col-xs-6">
                    <div class="number">
                        <i class="fa fa-server"></i>
                        <h3 class="white-text"><span class="counter">{{ count($servers) }}</span></h3>
                        <span class="white-text">{{ __('numbers.servers') }}</span>
                    </div>
                </div>
                <!-- /number -->

                <!-- number -->
                <div class="col-sm-3 col-xs-6">
                    <div class="number">
                        <i class="fa fa-trophy"></i>
                        <h3 class="white-text"><span class="counter">{{ env('NR_TOURNAMENTS') }}</span></h3>
                        <span class="white-text">
                            @if((int)env('NR_TOURNAMENTS') > 1)
                                {{ __('numbers.tornaument')."s" }} 
                            @else
                                {{ __('numbers.tornaument') }}
                            @endif
                        </span>
                    </div>
                </div>
                <!-- /number -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

        </div>
        <!-- /Numbers -->

        <!-- Pricing 
        <div id="pricing" class="section md-padding">

            
            <div class="container">

                
                <div class="row">

                    
                    <div class="section-header text-center">
                        <h2 class="title">{{ __('pricing.title') }}</h2>
                    </div>
                    

                    
                    <div class="col-sm-6">
                        <div class="pricing">
                            <div class="price-head">
                                <span class="price-title">{{ __('pricing.vip.title') }}</span>
                                <div class="price">
                                    <h3>{{ env('VIP_PRICE') }}€<span class="duration">/ {{ __('pricing.yearly') }}</span></h3>
                                </div>
                            </div>
                            <ul class="price-content">
                                @foreach(__('pricing.vip.features') as $feature)
                                    <li>
                                        <p>{{ $feature }}</p>
                                    </li>
                                @endforeach
                                <li>
                                    <p>...</p>
                                </li>
                            </ul>
                            <div class="price-btn">
                                <a target="_blank" href="https://steamcommunity.com/groups/DesuCommunity/discussions/0/4379130600468097088/">
                                    <button class="outline-btn">{{ __('pricing.buy_now') }}</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="pricing sponsor">
                            <div class="price-head">
                                <span class="price-title">{{ __('pricing.sponsor.title') }}</span>
                                <div class="price">
                                    <h3>{{ env('SPONSOR_PRICE') }}€<span class="duration">/ {{ __('pricing.monthly') }}</span></h3>
                                </div>
                            </div>
                            <ul class="price-content">
                                @foreach(__('pricing.sponsor.features') as $feature)
                                    <li>
                                        <p>{{ $feature }}</p>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="price-btn">
                                <a target="_blank" href="https://steamcommunity.com/groups/DesuCommunity/discussions/0/4379130600468097088/">
                                    <button class="outline-btn">{{ __('pricing.buy_now') }}</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    

                </div>
                

            </div>
           

        </div>
        -->

        <!-- Team -->
        <div id="team" class="section md-padding">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- Section header -->
                <div class="section-header text-center">
                    <h2 class="title">{{ __('team.title') }}</h2>
                </div>
                <!-- /Section header -->

                <!-- team -->
                @if($teamMembers && count($teamMembers) > 0)
                    @foreach($teamMembers as $teamMember)
                    <div class="col-sm-4">
                        <div class="team">
                            <div class="team-img">
                                <img class="img-responsive" src="{{ $teamMember['avatar'] }}" alt="">
                                <div class="overlay">
                                    <div class="team-social">
                                        <a href="{{ $teamMember['steamurl'] }}" target="_blank"><i class="fa fa-steam"></i></a>
                                    </div>
							    </div>
                            </div>
                            <div class="team-content">
                                <h3>{{ $teamMember['name'] }}</h3>
                                <span>{{ __('team.roles.' . $teamMember['role']) }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                <!-- /team -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

        </div>
        <!-- /Team -->  

        <!-- Testimonial
        <div id="testimonial" class="section md-padding">

        
        <div class="bg-img" style="background-image: url('./img/background3.jpg');">
            <div class="overlay"></div>
        </div>
        
        <div class="container">

            
            <div class="row">

                
                <div class="col-md-10 col-md-offset-1">
                    <div id="testimonial-slider" class="owl-carousel owl-theme">

                        
                        <div class="testimonial">
                            <div class="testimonial-meta">
                                <img src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/8b/8b3f4677c12b61055cd1e88b31469c47ca20b797_full.jpg" alt="">
                                <h3 class="white-text">RogerBot</h3>
                                <span>Mapper e Jogador de Courses/Bhop</span>
                            </div>
                            <p class="white-text">Comunidade com servidores únicos e com capacidade para melhorar a cada dia.</p>
                        </div>

                        <div class="testimonial">
                            <div class="testimonial-meta">
                                <img src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/43/433b3bb4564f7e7066e1196143f2b93b1323c996_full.jpg" alt="">
                                <h3 class="white-text">WhiteR</h3>
                                <span>Jogador de Courses/Bhop</span>
                            </div>
                            <p class="white-text">Comunidade com servidores únicos e com capacidade para melhorar a cada dia.</p>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        </div>
        -->

@endsection