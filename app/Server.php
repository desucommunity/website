<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $connection = 'servers_mysql';

    protected $table = 'csgo_serverlist';

    public function getIPAddressAttribute() 
    {
        switch($this->id) {
            case 1:
                return "185.113.141.12"; //courses
            case 2:
                return "185.113.141.3"; //surf combat
            default:
                return "";
        }
    }
}
