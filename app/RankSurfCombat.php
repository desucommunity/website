<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;
use DB;

class RankSurfCombat extends Model
{
    protected $connection = 'rank_surfcombat_mysql';

    protected $table = 'rankme';

    protected $ranks = [
        ["points" => 2700, "image" => "ge.png"],
        ["points" => 2400, "image" => "smfc.png"],
        ["points" => 2200, "image" => "lem.png"],
        ["points" => 2000, "image" => "le.png"],
        ["points" => 1800, "image" => "dmg.png"],
        ["points" => 1600, "image" => "mge.png"],
        ["points" => 1400, "image" => "mg2.png"],
        ["points" => 1200, "image" => "mg1.png"],
        ["points" => 1050, "image" => "g4.png"],
        ["points" => 900, "image" => "g3.png"],
        ["points" => 750, "image" => "g2.png"],
        ["points" => 600, "image" => "g1.png"],
        ["points" => 500, "image" => "sem.png"],
        ["points" => 400, "image" => "se.png"],
        ["points" => 300, "image" => "s4.png"],
        ["points" => 200, "image" => "s3.png"],
        ["points" => 150, "image" => "s2.png"],
        ["points" => 100, "image" => "s1.png"],
    ];

    public function getKDRAttribute()
    {
        if($this->deaths == 0)
            return 0;

        return round($this->kills/$this->deaths, 2);
    }

    public function getRankImageAttribute()
    {
        foreach($this->ranks as $rank) {
            if($this->score >= $rank["points"]) {
                return $rank["image"];
            }
        }

        $lastKey = key(array_slice($this->ranks, -1, 1, true));
        return $this->ranks[$lastKey]["image"];
    }

    public function scopeWithRowNumber($query, $column = 'score', $order = 'desc'){ $sub = static::selectRaw('*, row_number() OVER () as row_number') ->orderBy($column, $order) ->toSql(); $query->from(DB::raw("({$sub}) as sub")); }
}