<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server;
use DB;
use Http;
use Cache;

class WelcomeController extends Controller
{
    public function index()
    {
        $servers = Server::all();

        $playersOnline = Server::select(DB::raw('SUM(number_of_players) as total_players'))->first();
        $steamGroupMembers = 0;
        
        if(!is_null($playersOnline)) {
            $playersOnline = $playersOnline->total_players;
        }else {
            $playersOnline = 0;
        }

        if (!Cache::has('teamMembers')) {

            $teamMembers = $this->getTeamMembers();
            if(!is_null($teamMembers)) {
                Cache::put('teamMembers', $teamMembers, 3600); //3600seg - 1 hora
            }
            
        }else {
            $teamMembers = Cache::get('teamMembers');
        }
        
        if (!Cache::has('steamGroupMembers')) {

            $steamGroupMembers = $this->getSteamGroupMembers();
            if(!is_null($steamGroupMembers)) {
                Cache::put('steamGroupMembers', $steamGroupMembers, 3600); //3600seg - 1 hora
            }
            
        }else {
            $steamGroupMembers = Cache::get('steamGroupMembers');
        }
        
        return view('welcome')->with(compact('servers','playersOnline','steamGroupMembers', 'teamMembers'));
    }

    private function getSteamGroupMembers() 
    {
        $steamGroupName = env('STEAMGROUP_NAME');

        $url = "https://steamcommunity.com/groups/{$steamGroupName}/memberslistxml/?xml=1&p=1";

        $response = Http::get($url);
    
        if($response->ok()) {

            if (preg_match('/<memberCount>(.*?)<\/memberCount>/', $response->body(), $match) == 1) {
                return $match[1];
            }
        }
        
        return null;
    }

    private function getTeamMembers()
    {
        $team = [
            ["steamid" => "76561198869953245", "role" => "founder"], //reygan
            ["steamid" => "76561197991583292", "role" => "founder"], //trayz
            ["steamid" => "76561198150658575", "role" => "founder"], //shido
            ["steamid" => "76561198153569994", "role" => "admin"], //p1c4
            ["steamid" => "76561198208471040", "role" => "mod"], //slump
            ["steamid" => "76561198092318909", "role" => "mod"], //roger
            ["steamid" => "76561198197707561", "role" => "mod"], //zero
            ["steamid" => "76561198194446155", "role" => "mod"], //neek
            ["steamid" => "76561198120310497", "role" => "mod"], //deathz
            ["steamid" => "76561198089070766", "role" => "mod"], //whiter
            ["steamid" => "76561198326996759", "role" => "mod"], //trigo
            ["steamid" => "76561199056240697", "role" => "helper"], //btk
            
        ];

        $steamIds = implode(",", array_column($team, "steamid"));

        $steamApiKey = env('STEAM_API_KEY');
        $url = "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={$steamApiKey}&steamids={$steamIds}";

        $response = Http::get($url);
    
        if($response->ok()) {

            $data = $response->json();

            if(!is_null($data) && isset($data["response"])) {

                foreach($data["response"]["players"] as $player) {

                    foreach($team as $key => $teamPlayer) {

                        if($teamPlayer["steamid"] == $player["steamid"]) {
                            
                            $team[$key]["name"] = $player["personaname"];
                            $team[$key]["avatar"] = $player["avatarfull"];
                            $team[$key]["steamurl"] = $player["profileurl"];
                            
                        }

                    }

                }

                return $team;

            }
            
        }

        return null;
    }
}
