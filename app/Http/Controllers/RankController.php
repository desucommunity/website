<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RankSurfCombat;
use SteamID;
use App\RankCourseBhop\Rank;


class RankController extends Controller
{
    public function surf_combat()
    {
        $players = RankSurfCombat::orderBy('score', 'DESC')->paginate(10);

        $playerLogin = auth()->guard('player')->user();

        $page = request()->get('page') ? request()->get('page')-1 : 0;

        $numRows = RankSurfCombat::count();

        if($playerLogin) {
            $s = new SteamID($playerLogin->steamid);
            $steamID2 = $s->RenderSteam2();
            $playerLogin = RankSurfCombat::where('steam', $steamID2)->first();
        }

        return view('ranks.surf-combat')->with(compact('players','playerLogin','page','numRows'));
    }

    public function course_bhop()
    {
        $players = Rank::orderBy('cachedpoints', 'desc')->with('user')->paginate(10);

        $playerLogin = auth()->guard('player')->user();

        $page = request()->get('page') ? request()->get('page')-1 : 0;

        $numRows = Rank::count();

        if($playerLogin) {
            $s = new SteamID($playerLogin->steamid);
            $steamID3 = $s->RenderSteam3();
            $playerLogin = Rank::whereHas('user', function ($query) use($steamID3) {
                return $query->where('steamid', $steamID3);
            })->first();
            
        }

        return view('ranks.course-bhop')->with(compact('players','playerLogin','page','numRows'));
    }
}
