<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use kanalumaddela\LaravelSteamLogin\Http\Controllers\AbstractSteamLoginController;
use kanalumaddela\LaravelSteamLogin\SteamUser;
use App\Player;

class SteamLoginController extends AbstractSteamLoginController
{
    /**
     * {@inheritdoc}
     */
    public function authenticated(Request $request, SteamUser $steamUser)
    {
        $player = Player::where('steamid', $steamUser->steamId)->first();

        // if the user doesn't exist, create them
        if (!$player) {

            $steamUser->getUserInfo();

            $player = Player::create([
                'name' => $steamUser->name, // personaname
                'steamid' => $steamUser->steamId,
                'avatar' => $steamUser->avatar
            ]);
        }
        
        auth()->guard('player')->login($player);
        return redirect()->route('homepage');
    }

    public function logout()
    {
        auth()->guard('player')->logout();
        return redirect()->route('homepage');
    }
}