<?php

namespace App\RankCourseBhop;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $connection = 'rank_coursebhop_mysql';

    protected $primaryKey = 'uid';

    protected $table = 'inf_simpleranks';

    public function user()
    {
        return $this->belongsTo('App\RankCourseBhop\User', 'uid', 'uid');
    }

    public function getrankAttribute()
    {
        $replaceTags = [
            "{WHITE}" => '',
            "{BLUE}" => '',
            "{GREY}" => '',
            "{RED}" => '',
            "{GREEN}" => '',
            "{SKYBLUE}" => '',
            "{LIGHTRED}" => '',
            "{GOLD}" => '',
            "{LIGHTGREEN}" => '',
            "{PINK}" => '',
        ];
        return strtr($this->chosenrank, $replaceTags);
    }
}