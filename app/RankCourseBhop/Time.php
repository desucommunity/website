<?php

namespace App\RankCourseBhop;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $connection = 'rank_coursebhop_mysql';

    protected $primaryKey = 'uid';

    protected $table = 'inf_times';

    public function map()
    {
        return $this->belongsTo('App\RankCourseBhop\Map', 'mapid', 'mapid');
    }

    public function user()
    {
        return $this->belongsTo('App\RankCourseBhop\User', 'uid', 'uid');
    }
}