<?php

namespace App\RankCourseBhop;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'rank_coursebhop_mysql';

    protected $primaryKey = 'uid';

    protected $table = 'inf_users';
}