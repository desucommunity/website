<?php

namespace App\RankCourseBhop;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $connection = 'rank_coursebhop_mysql';

    protected $primaryKey = 'mapid';

    protected $table = 'inf_maps';
}