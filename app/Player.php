<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Player extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'steamid',
        'name',
        'avatar'
    ];
}
