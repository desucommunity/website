<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\SteamLoginController;
use kanalumaddela\LaravelSteamLogin\Facades\SteamLogin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('homepage');

Route::get('/ranks/surf-combat', 'RankController@surf_combat')->name('ranks.surf-combat');

Route::get('/ranks/courses-bhop', 'RankController@course_bhop')->name('ranks.courses-bhop');

Route::get('/donate', function() {
    return view('donate');
})->name('donate');

Route::get('lang/{locale}', 'LocalizationController@index')
->where('locale', 'pt|en');

SteamLogin::routes(['controller' => SteamLoginController::class]);
Route::get('logout', 'Auth\SteamLoginController@logout');

Route::fallback(function () {
    return view('404');
});